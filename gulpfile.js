var gulp = require('gulp');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var uglifyjs = require('gulp-uglifyjs');

gulp.task('css', function () {
    gulp.src('www/style/*.css')
    .pipe(concat('style.min.css'))
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest('www/dist'));
});

gulp.task('js', function () {
  gulp.src('www/script/*.js')
  .pipe(uglifyjs('script.min.js', {
      outSourceMap: false
    }))
  .pipe(gulp.dest('www/dist'));
});

gulp.task('default',['css', 'js']);

gulp.watch('www/style/*.css', function() {
  return gulp.run('css');
});

gulp.watch('www/script/*.js', function() {
  return gulp.run('js');
});
