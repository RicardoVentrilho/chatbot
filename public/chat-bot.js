function tiraAcento(nome) {
  nome = nome.toLowerCase();
  var novoNome = '';
	for (var i = 0; i < nome.length; i++) {
		if(nome[i] == 'à' || nome[i] == 'á' || nome[i] == 'â' || nome[i] == 'ã')
		{
			novoNome += 'a';
		} else
		if(nome[i] == 'ó' || nome[i] == 'õ' || nome[i] == 'ô')
		{
			novoNome += 'o';
		} else
		if(nome[i] == 'é' || nome[i] == 'ê')
		{
			novoNome += 'e';
		} else
		if(nome[i] == 'í')
		{
			novoNome += 'i';
		} else
		if(nome[i] == 'ú')
		{
			novoNome += 'u';
		} else {
      novoNome += nome[i];
    }
	}
	return novoNome;
}

function respondTo(input) {
	input = input.toLowerCase();
	input = tiraAcento(input);
  if(RegExp('(definicao|definição)(\\s|\\.|de)*(?=impressora)').test(input))
    return "Impressora é um periférico que, quando conectado a um computador ou a uma rede de computadores, tem a função de dispositivo de saída, imprimindo textos, gráficos ou qualquer outro resultado de uma aplicação.";

  if(RegExp('(tipo|tipos)(\\s|\\.|de)*(?=impressora|impressoras)').test(input))
  	return "Impressora de impacto, Impressora de jato de tinta,Impressora a laser,Impressora térmica, Impressora Solvente";

  if(RegExp('(impressora|impressoras)(\\s|\\.|não|nao)*(?=funciona)').test(input))
    return "Verifique se ela esta ligada ou se o cabo Usb esta conectado corretamente ao computador.";

  if(RegExp('(oi|ola|hei|ou|opa|alo)(\\s|!|\\.|$)').test(input)){
    switch(Math.floor((Math.random() * 3))) {
    case 0:
        return " Ola em que posso ajudar?";
        break;
    case 1:
        return "Oi é um prazer falar com voce";
        break;
    case 2:
        return "Oi eu sou o ChatterBot";
        break;
      }
  }

	if(RegExp('(help|ajuda)').test(input))
		return "<a href=# onclick=\"abrirHelp();\">Clique aqui para mais informações sobre mim.</a>";

  if(RegExp('(random)').test(input))
  	return Math.floor((Math.random() * 10) + 1);;

	if(RegExp('(\\s|^)(troxa|porra|vtnc|cú|cu|desgraça|poha|buceta|pinto|penis|caralho|inferno|satanas|puta|rapariga|vadia|vagabundo|burro|idiota|se fuder|te fuder)(\\s|$|\\.)').test(input))
		return " Voce parece estar Exaltado. Nao fale isso novamente, por favor!";



	return "Não entendi a expressão '" + input + "' . <a href=# onclick=\"abrirAprendizado();\">Clique aqui para ensinar o chat</a>";
}

module.exports = {
      respondTo : respondTo
 };
