function tiraAcento(nome) {
  nome = nome.toLowerCase();
  var novoNome = '';
	for (var i = 0; i < nome.length; i++) {
		if(nome[i] == 'à' || nome[i] == 'á' || nome[i] == 'â' || nome[i] == 'ã')
		{
			novoNome += 'a';
		} else
		if(nome[i] == 'ó' || nome[i] == 'õ' || nome[i] == 'ô')
		{
			novoNome += 'o';
		} else
		if(nome[i] == 'é' || nome[i] == 'ê')
		{
			novoNome += 'e';
		} else
		if(nome[i] == 'í')
		{
			novoNome += 'i';
		} else
		if(nome[i] == 'ú')
		{
			novoNome += 'u';
		} else {
      novoNome += nome[i];
    }
	}
	return novoNome;
}

var Enums = {
  tecla : {
    ENTER : 13
  }
}
var socket = io.connect('/');

function enviaMensagem() {
  var message = $('#mensagem').val();
  message = tiraAcento(message);
  $('#mensagem').val('');
  socket.emit('sendchat', message);
}

$(function() {

  $('.caixaDeAprendizado').hide();
  $('.caixaDeAjuda').hide();

  $('#btn_mensagem').click(function () {
    enviaMensagem();
  });

  $('#mensagem').keypress(function (e) {
    if (e.which == Enums.tecla.ENTER) {
      enviaMensagem();
    }
  });

});

socket.on('updatechat', function (username, message) {
  $('#conversa').append('<tr><td><b>' + username + ' disse: </b>' + message + '</td></tr>');
});

socket.on('learnchat', function (palavra, resposta) {
  alert('Obrigado por me ensinar.');
});

function abrirAprendizado() {
  if($('.caixaDeAprendizado').css('display') == 'none') {
    $('.caixaDeAprendizado').show();
  } else {
    $('.caixaDeAprendizado').hide();
  }
}

function ensinar() {
  var palavra = $('#palavrachave').val();
  var resposta = $('#respostachave').val();
  $('#palavrachave').val('');
  $('#respostachave').val('');
  socket.emit('learnchat', palavra, resposta);
}

function abrirHelp() {
  if($('.caixaDeAjuda').css('display') == 'none') {
    $('.caixaDeAjuda').show();
  } else {
    $('.caixaDeAjuda').hide();
  }
}
