/*
 * Definição de variáveis e módulos
 */
var diretorioRaiz = __dirname;
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bot = require('./public/chat-bot.js');

var fs = require('fs');
var obj = JSON.parse(fs.readFileSync('./public/data.json', 'utf8'));
/*
 * RouterMap - Definindo as rotas
 */
app.use('/public', express.static('public'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(diretorioRaiz + '/www/dist'));
app.use('/css', express.static(diretorioRaiz + '/www/dist'));

app.get('/', function(req, res) {
	var dir = '/www';
	var arquivo = '/index.html';
	res.sendFile(diretorioRaiz + dir + arquivo);
});

/*
 * Famoso socket.io
 */
io.on('connection', function(socket){
	console.log('Usuario conectado: '+socket.id);

	socket.on('disconnect', () => {
		console.log('Usuario desconectado: '+socket.id);
    });

	socket.on('sendchat', (message) => {
		var reply;
		/*
		*Leitura de arquivo JSON
		*/
		for(var i = 0; i < obj.length; i++) {
			if(RegExp('^'+obj[i].palavra+'$').test(message.toLowerCase())){
				reply = obj[i].resposta;
				break;
			}
		};

		console.log('Usuario:'+socket.id+' diz:'+message);
		io.to(socket.id).emit('updatechat', 'Voce', message);
		if (reply == null)
		  reply = bot.respondTo(message);

		setTimeout(function() {
			io.to(socket.id).emit('updatechat', 'chat', reply)
		},300);

	 });

   /*
	 * Escrita no arquivo JSON
	 */
	 socket.on('learnchat', function(p, r) {
 		console.log('gravando');
		obj.push({palavra: p, resposta : r}); //add some data
		fs.writeFile('./public/data.json', JSON.stringify(obj) );
		io.to(socket.id).emit('learnchat', p, r);
		console.log('sucesso');
 	 });

});

http.listen(8081, function() {
	console.log('Executando o servidor na porta 8081...');
});
