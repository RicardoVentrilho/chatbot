# **Chatbot** #

Criação de um chatbot para fins acadêmicos.

# Inicialização #

Instale o Nodejs.

Com o Nodejs instalado, na pasta do projeto, no terminal digite:

```
#!npm

npm install --g gulp

```
Agora você instalou o gulp como global, agora digite:

```
#!npm

npm install

```
Rode o gulp, com o comando:

```
#!npm

gulp
```
Com o gulp em execução, este ficará observando as modificações do css (www/style) e javascript (www/script) concatenando e minificando os arquivos. Os arquivos concatenados e minificados estarão na pasta www/dist.


# Estrutura do projeto: #


```
#!
 --vendor
 --www
   --dist
   --script
   --style
 --public
 --README
 --server.js
 --gulfile.js
```
